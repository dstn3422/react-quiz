import React from "react";
import ReactDOM from "react-dom";
import QuestionCard from "../QuestionCard";
/* import { isTSAnyKeyword } from "@babel/types"; */
import { render } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';

it('renders question card without crashing', () => {
    const div = document.createElement('div');
    ReactDOM.render(
        <QuestionCard
        questionNumber={0}
        totalQuestions={10}
        question={'Isn\'t react cool?'}
        answers={['yes it is','indeed','ofc','yup']}
        userAnswer={undefined}
        callback={() => true}
        />, div);
});

it('renders question card correctly', () => {
    const {getByTestId} = render(
        <QuestionCard
        questionNumber={0}
        totalQuestions={10}
        question={'Isn\'t react cool?'}
        answers={['yes it is','indeed','ofc','yup']}
        userAnswer={undefined}
        callback={() => true}
        />
    );
    const questionCardNumber = getByTestId('question-card__number');
    expect(getByTestId('question-card')).toContainElement(questionCardNumber);
});